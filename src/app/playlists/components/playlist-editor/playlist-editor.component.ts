import { Component, Input, OnInit } from '@angular/core';
import { PlayList } from '../../Models/PlayList';

@Component({
  selector: 'app-playlist-editor',
  templateUrl: './playlist-editor.component.html',
  styleUrls: ['./playlist-editor.component.scss']
})
export class PlaylistEditorComponent implements OnInit {

  @Input() selected?: PlayList

  constructor() { }

  ngOnInit(): void {
  }

}
