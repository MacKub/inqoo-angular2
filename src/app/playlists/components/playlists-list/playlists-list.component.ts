import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PlayList } from '../../Models/PlayList';

@Component({
  selector: 'app-playlists-list',
  templateUrl: './playlists-list.component.html',
  styleUrls: ['./playlists-list.component.scss']
})
export class PlaylistsListComponent implements OnInit {

  @Input() selected?: PlayList

  @Input()  playlists: PlayList[] = []

  @Output() selectedChange = new EventEmitter<PlayList>()

  select(playlist: PlayList){ 
    if (playlist.id === this.selected?.id){
      this.selectedChange.emit(undefined)
    } else this.selectedChange.emit(playlist)
  }

  constructor() { }

  ngOnInit(): void {
  }

}
