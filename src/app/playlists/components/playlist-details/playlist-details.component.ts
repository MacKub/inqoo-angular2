import { Component, Input, OnInit } from '@angular/core';
import { PlayList } from '../../Models/PlayList';

@Component({
  selector: 'app-playlist-details',
  templateUrl: './playlist-details.component.html',
  styleUrls: ['./playlist-details.component.scss']
})
export class PlaylistDetailsComponent implements OnInit {

  @Input() selected?: PlayList

  constructor() { }

  ngOnInit(): void {
  }

}
