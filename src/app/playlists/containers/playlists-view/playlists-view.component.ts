import { Component, OnInit } from '@angular/core';
import { PlayList } from '../../Models/PlayList';

@Component({
  selector: 'app-playlists-view',
  templateUrl: './playlists-view.component.html',
  styleUrls: ['./playlists-view.component.scss']
})
export class PlaylistsViewComponent implements OnInit {

  mode: 'details' | 'edit' = 'details'

  
  playlists: PlayList[] = [{
    id: '123',
    name: 'Playlist 123',
    public: true,
    description: 'my favourite playlist'
  }, {
    id: '234',
    name: 'Playlist 234',
    public: false,
    description: 'my favourite playlist'
  }, {
    id: '345',
    name: 'Playlist 34',
    public: true,
    description: 'my favourite playlist'
  }]
  
  selected?: PlayList = this.playlists[2]

  constructor() { }

  edit() {
    this.mode = 'edit'
  }
    
  cancel(){
    this.mode = 'details'
  }

  ngOnInit(): void {
  }

}
