export interface PlayList {
  id: string;
  name: string;
  public: boolean;
  description: string;
}
