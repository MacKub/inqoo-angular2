## GIT 
git clone https://mateuszkulesza@bitbucket.org/mateuszkulesza/inqoo-angular2.git

cd inqoo-angular2
npm i  
<!-- albo kopia node_modules z zipa https://we.tl/t-oSoCkSlAPA -->
npm start

## Instalacje

npm install -g @angular/cli
ng --version
ng.cmd --version

Angular CLI: 12.2.7

ng new  --directory . --strict

? What name would you like to use for the new workspace and initial project? inqoo-ng2
? Would you like to add Angular routing? Yes
? Which stylesheet format would you like to use? SCSS   [ https://sass-lang.com/documentation/syntax#scss []

ng new --directory . --name inqoo-ng2 --strict --style scss --routing

npm i bootstrap 

## Start developer server 
npm start
<!-- or -->
ng serve -o

open > http://localhost:4200/

## Node modules ZIP
https://we.tl/t-oSoCkSlAPA

https://we.tl/t- oS oC kS lA PA

